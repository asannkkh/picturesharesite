order = "";
$(document).ready(function () {
	$.ajax({
		type: "get",
		url: "order",
		dataType: "json",
		success: function (data, dataType) {
			if (!data.order) {
				location.href = "login.html";
			}else{
				order = data.order;
				//注文がある
				document.title = "ご注文は" + order +"ですか？？";
				$(".navbar-brand").html("ご注文は" + order +"ですか？？");
			}
		}
	});
});

function GetCookies() {
	var result = new Array();
	var allcookies = document.cookie;
	if (allcookies != '') {
		var cookies = allcookies.split('; ');
		for (var i = 0; i < cookies.length; i++) {
			var cookie = cookies[i].split('=');
			// クッキーの名前をキーとして 配列に追加する
			result[cookie[0]] = decodeURIComponent(cookie[1]);
		}
	}
	return result;
}