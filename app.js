var express = require('express');
var Database = require("nedb");
var path = require("path");
var uuid = require('node-uuid');
var fs = require("fs");
var ExifImage = require('exif').ExifImage;
var JSZip = require("jszip");
var async = require("async");
require('date-utils');
var multiparty = require("multiparty");
var bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
var session = require('express-session');
var NedbStore = require('nedb-session-store')(session);
var fileType = require('file-type');
var ffmpeg = require('fluent-ffmpeg');
var readChunk = require('read-chunk');
var db = new Database({
	filename: "./data/post"
});
var easyimg = require('easyimage');

var zipQue = [];

//データベース
db.loadDatabase(function (err) {});

var app = express();
app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(bodyParser.json());
app.use(cookieParser()); // cookieのパースを行う。次に決めるsecretを使って行われる
app.use(session({ // cookieに書き込むsessionの仕様を定める
	secret: 'secret', // 符号化。改ざんを防ぐ
	store: new NedbStore({
		filename: 'data/session'
	}),
	resave: true,
	saveUninitialized: true,
	cookie: { //cookieのデフォルト内容
		httpOnly: false,
		maxAge: 60 * 60 * 1000 // ★★修正箇所：1 hour. ここを指定しないと、ブラウザデフォルト(ブラウザを終了したらクッキーが消滅する)になる こちらはms
	}
}));
var loginCheck = function (req, res, next) {
	if (req.session.order) {
		next();
	} else {
		res.redirect('login.html');
	}
};
app.use(express.static(path.join(__dirname, 'public')));

var multer = require('multer');
var upload = multer({
	dest: './tmp',
	putSingleFilesInArray: true,
	limits: {
		fieldNameSize: 1000,
		fileSize: 1024 * 1024 * 1024 * 4
	}
});

app.get('/', function (req, res) {
	res.send('Hello, World!');
});
app.get('/order', function (req, res) {
	res.send({
		order: req.session.order
	});
})
app.post('/login', function (req, res) {
	console.log("login");
	console.log("ご注文は" + req.body.order + "ですか？？");
	req.session.order = req.body.order;
	res.redirect("/");
});
app.get('/logout', function (req, res) {
	console.log("logout");
	console.log('deleted sesstion');
	req.session.destroy(function (err) {
		if (err) throw err;
	});
	res.redirect('/');
});

app.get('/data', function (req, res) {
	console.log("検索");
	db.find({
		order: req.session.order
	}).sort({
		date: 1
	}).exec(function (err, docs) {
		if (err) throw err;
		res.contentType('application/json');
		res.send(docs);
	});
})
app.post('/upload', function (req, res) {
	//	var que = [];
	var form;
	form = new multiparty.Form({
		uploadDir: __dirname + "/tmp"
	});
	var filelist = [];
	form.parse(req, function (err, fields, files) {
		console.log(files.select_files);
		async.eachSeries(files.select_files, function (filedata, next) {
			console.log(filedata);
			var filename = uuid.v4().split('-').join('') + filedata.originalFilename
			var newPath = __dirname + "/public/upload/full/" + filename;
			fs.rename(filedata.path, newPath, function (err) {
				if (err) throw err;
				//ファイルの種類を判別
				var buffer = readChunk.sync(newPath, 0, 262);
				var type = fileType(buffer);
				console.log(type);
				//サムネイル作成
				console.log("サムネイル作成");
				if(type.mime.indexOf("image") >= 0){
					//画像
					easyimg.rescrop({
						src: newPath,
						dst: __dirname + "/public/upload/thumb/" + filename,
						width: 200
					}).then(
						function (image) {
							console.log('Resized and cropped: ' + image.width + ' x ' + image.height);
							//DB登録
							try {
								new ExifImage({
									image: newPath
								}, function (error, exifData) {
									if (error) {
										console.log('Error: ' + error.message);
										db.insert({
											order: req.session.order,
											name: filename,
											date: 0
										});
									} else {
										db.insert({
											order: req.session.order,
											name: filename,
											date: exifData.exif.DateTimeOriginal,
											mime:type.mime
										});
									}
									filelist.push(filename);
									next();
								});
							} catch (error) {
								console.log('Error: ' + error.message);
								next();
							}
						},
						function (err) {
							console.log(err);
							next();
						}
					);
				}else if(type.mime.indexOf("video") >= 0){
					//動画
					try{
					var command = ffmpeg(newPath);
					command.on('end', () => {
						fs.rename(__dirname + "/public/tmp/" + filename + ".jpg", __dirname + "/public/upload/thumb/" + filename, function (err) {
							db.insert({
								order: req.session.order,
								name: filename,
								date: 0,
								mime:type.mime
							});
							filelist.push(filename);
							next();
						})
					}).screenshots({
						timemarks: [0],
						folder: __dirname + "/public/tmp/" ,
						filename: filename + ".jpg",
						size: '200x?'
					});
					}catch(error){
						console.log('Error: ' + error.message);
						next();
					}
				}
				
			});
		}, function complete(err) {
			console.log('all upload!');
			zipQue.push({
				order : req.session.order,
				list : filelist
			});
		});
		res.sendStatus(200);
	});
});
app.post('/downloadallzip', function (req, res) {
	res.send({
		uri: "upload/zip/tag_" + new Buffer(req.session.order).toString('base64') + ".zip"
	});
});
app.post('/downloadzip', function (req, res) {
	var zip = new JSZip();
	var img = zip.folder("images");
	var filename = uuid.v4().split('-').join('') + ".zip";
	res.send({
		uri: "upload/zip/" + filename
	});
	async.each(req.body.request, function (data, next) {
		if (data.indexOf("/") != -1) {
			console.log("パスがおかしい");
			next();
			return;
		} else {
			try {
				fs.readFile(__dirname + "/public/upload/full/" + data, function (err, contents) {
					if (err) throw err;
					var filename = data;
					console.log("loaded" + filename);
					try {
						//ファイル名を決める
						new ExifImage({
							image: __dirname + "/public/upload/full/" + data
						}, function (error, exifData) {
							if (error) {
								console.log('Error: ' + error.message);
							} else {
								var exp = path.extname(data);
								if(exifData.exif.DateTimeOriginal){
									var exp = path.extname(data);
									filename = new Date(exifData.exif.DateTimeOriginal.replace(":","/").replace(":","/")).toFormat("YYYYMMDD_HH24_MI_SS_") + path.basename(data,exp).substr(32) + exp;
								}else{
									img.file(filename, contents);
								}
							}
							img.file(filename, contents);
							next();
						});
					} catch (error) {
						console.log('Error: ' + error.message);
						img.file(filename, contents);
						next();
					}
				})
			} catch (e) {
				console.log(e);
			}
		}
	}, function complete(err) {
		console.log('all done!');
		var buffer = zip.generate({
			type: "nodebuffer"
		});
		fs.writeFile(__dirname + "/public/upload/zip/" + filename, buffer, function (err) {
			if (err) throw err;
			console.log("Zip File Done");
		});
	});
});
app.post('/zipexist',function(req,res){
	fs.stat("./public/upload/zip/" + req.body.filename.match(".+/(.+?)([\?#;].*)?$")[1], function (err, stats) {
		//if (err) throw err;
		console.log("file "+req.body.filename.match(".+/(.+?)([\?#;].*)?$")[1]+ " " +(!err));
		res.send({
			stat: !err
		});
	});
})

checkQue();
function checkQue(){
	if(zipQue.length > 0){
		//キューがある
		addFile(zipQue[0]);
		zipQue.shift();
	}else{
		setTimeQue();
	}
}

function setTimeQue(){
	setTimeout(function (){
		checkQue();
	},1000);
};
function addFile(datalist){
	var order = datalist.order;
	zipfilename = __dirname + "/public/upload/zip/tag_" + new Buffer(order).toString('base64') + ".zip";
	console.log("addFile name:" + zipfilename);;
	fs.readFile(zipfilename, function (err, bin) {
		var zip;
		if (!err){
			zip = new JSZip(bin);
		}else{
			console.log("zipファイル新規作成");
			zip = new JSZip();
		}
		//追加処理
		var img = zip.folder("images");
		async.each(datalist.list, function (data, next) {
			if (data.indexOf("/") != -1) {
				console.log("パスがおかしい");
				next();
				return;
			} else {
				try {
					fs.readFile(__dirname + "/public/upload/full/" + data, function (err, contents) {
						if (err) throw err;
						var filename = data;
						console.log("loaded" + filename);
						try {
							//ファイル名を決める
							new ExifImage({
								image: __dirname + "/public/upload/full/" + data
							}, function (error, exifData) {
								if (error) {
									console.log('Error: ' + error.message);
								} else {
									if(exifData.exif.DateTimeOriginal){
										var exp = path.extname(data);
										filename = new Date(exifData.exif.DateTimeOriginal.replace(":","/").replace(":","/")).toFormat("YYYYMMDD_HH24_MI_SS_") + path.basename(data,exp).substr(32) + exp;
									}else{
										img.file(filename, contents);
									}
								}
								img.file(filename, contents);
								next();
							});
						} catch (error) {
							console.log('Error: ' + error.message);
							img.file(filename, contents);
							next();
						}
					})
				} catch (e) {
					console.log(e);
				}
			}
		}, function complete(err) {
			console.log('all done!');
			var buffer = zip.generate({
				type: "nodebuffer"
			});
			fs.writeFile(zipfilename, buffer, function (err) {
				setTimeQue();
				if (err) throw err;
				console.log("Zip File Done");
			});
		});
		
	});
}

app.listen(3000);

function splitExt(filename) {
	return filename.split(/(?=\.[^.]+$)/);
}